# Scroll List By Kapil Kumawat
The idea of this project was to create infinite scrollable message list for mobile and user should scroll through messages smoothly and should be able to delete it using swipe.

![Demo of scroll list](media/app_demo.gif)

I am trying to build an progressive app, so I have hosted on surge, as its free as well has ssl support.

[Demo app](https://kkscl.surge.sh)

Audit Report:

![Audit Report](media/Audit.png)

Some issues pending right now:
* Handle WCAG issues
* Async Routs to reduce initial payloads
* Add Unit testing and complete code coverage

Coding principals I have used
* For javascript: Used components at each level, worked on smaller components which extensible
* For CSS: Used [BEM Methodology](http://getbem.com/introduction/)



## Dependencies
Project majorly depends on:
* **Preact** - Used because for its performance, light payload and template in project
* **Hammer.js** - Used to recognize gestures
* **webpack** - Used for bundling and packaging content
* **babel** - Used for ES6 code transpile to es2015 
* **sw-precache** - Used with webpack to configure service worker
* **Eslint** - Extended standard from Eslint standard and eslint-config-standard-preact
* **md-color** - To get material ui colors
* **preact-icons** - For icons library

## Thinking & Design Process
### Basic Idea and Ext Dependencies
My Idea for this assessment was to create responsive fast mobile web app, so I have selected to go for progressive web app. Now I have to select some framework for template and create smaller and reusable components, reason was simple to not to invent wheel and some good frameworks already available, also the time line  I have took into consideration. I have gone for [Preact](https://preactjs.com/) framework as it has very light payload along with it 3KB. Finally used webpack, babel and eslint for build configuration for there own advantage in it.

Other requirement was to create an Infinite scroll where messages can be swiped to delete, so I have to listen for swipe/drag other events for touch devices. For this I have selected [HammerJs](https://hammerjs.github.io/)  which will provide all this with a extra payload of 7.34KB.

For styling I have used SASS as preprocessor and other libraries like mdcolor and preact-router(for navigation) are also used.

### Layout and Components
Now layout is created as Layout and Headers, for this I have created two one for large screen and other for small like mobile. Smaller screens has Sidebar having links and Fixed header for label of the page and for bigger screen Used only fixed header all menus at top header.

![Payload](media/layout.gif)

Other Components were build like profile, snackbar and card, these are reused to create pages.

### Cards
Its an card view which used to render messages in the list. This card needs to swiped to delete the messages. This swipe transaction should be smooth, then only it can give you a clear UX to end user. 

I have a change for smooth transition, so I have divided in smaller problems in four stages:
* Start the drag - show some indication by lower opacity .8
* Drag right - transform the card to new position as user drags, after every new animation frame
* Dragged more then 25%, so delete confirmed (Indicating by lower opacity to .2) now user can see card is going to delete
* Drag end - Move card to full right end to show card removed using transition and after 500 milliseconds remove the card from the screen

This has few exceptions:
* User drags to right and hold, so undo the card at some position of start after one second
* User drags left and move back to right, so don't delete the card, so restore the card.

![Card](media/card.gif)

### Snackbar
Another important thing for user is to show notifications, for this I have created the Snackbar. Example for its use:
*   App goes offline
*   App comes online
*   Message deleted with undo that Action

### Message Communication
Instead of using any library with Preact like redux or flex, I have used Javascript native events for messaging.


**Note:** Complete payload of the App in production is 37KB, in which styles and javascript comes around 20 KB.
![Payload](media/payload.png)

