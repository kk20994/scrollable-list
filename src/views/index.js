import { h } from 'preact'
import { Router } from 'preact-router'

import Home from './pages/home'
import Messages from './pages/messages'
import About from './pages/about'
import Layout from './tags/layout'
import Error404 from './pages/errors/404'

// track pages on route change
const onChange = obj => window.ga && window.ga.send('pageview', { dp: obj.url })

// Component
export default (
    <Layout>
        <Router onChange={onChange}>
            <Home path='/' />
            <Messages path='/messages' />
            <About path='/about' />
            <Error404 default />
        </Router>
    </Layout>
)
