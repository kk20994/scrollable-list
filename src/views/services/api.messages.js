import config from '../config.json'
import {timeDifference} from '../util'

/**
 * SPI to fetch messages
 * @param {Int} limit (20)
 * @param {String} prevToken
 */
export function fetchMessages (limit = 20, prevToken) {
    return fetch(`${config.api.host}${config.api.messages}?limit=${limit}&pageToken=${prevToken}`)
        .then((response) => {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' + response.status)
                return
            }
            return response.json()
        })
        .then((data) => {
            // Transform the data
            data.messages = data.messages.map((message) => {
                if (message.author && message.author.photoUrl) {
                    message.author.photoUrl = config.api.host + message.author.photoUrl
                }
                if (message.updated) {
                    message.updated = timeDifference(message.updated)
                }
                return message
            })
            return data
        })
}
