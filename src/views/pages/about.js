import { h } from 'preact'
import Profile from '../tags/profile'

/**
 * Render About Page
*/
export default function (props) {
    const profileData = {
        name: 'Kapil Kumawat',
        img: '/images/kapil.jpg',
        title: 'User Interface Developer at ServiceNow',
        subTitle: 'UI Expert, 9+ Years Industry Experience, Hyderabad India'
    }
    return (
        <div class='page'>
            <Profile {...profileData} />
        </div>
    )
}
