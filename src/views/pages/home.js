import { h } from 'preact'
import { Link } from 'preact-router'
import Card from '../tags/card'
import FaList from 'preact-icons/lib/fa/list'
import FaUser from 'preact-icons/lib/fa/user'

/**
 * Render Homepage
 * @param {Object} props
 */
export default function (props) {
    return (
        <div className='page'>
            <Card>
                <h1>Home</h1>
                <p>This is the home page.</p>

                <p>You should check out:</p>
                <nav>
                    <div className='home-tile'>
                        <Link href='/messages'><FaList size={32} /> <span>Messages</span></Link>
                    </div>
                    <div className='home-tile'>
                        <Link href='/about'><FaUser size={32} /> <span>About Me</span></Link>
                    </div>
                </nav>
            </Card>
        </div>
    )
}
