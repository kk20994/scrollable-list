import { h, Component } from 'preact'
import Card from '../tags/card'
import { fetchMessages } from '../services/api.messages'
import { launchSnackbar } from '../util'

/**
 * Creates the Infinite Scroll list of messages
*/
export default class Messages extends Component {
    /**
     * Constructor of the class
     * @param {*} args
     */
    constructor (...args) {
        super(args)
        this.state = {
            messages: [],
            loadingData: true
        }
        this.removeMessage = this.removeMessage.bind(this)
        this.isErrorOccurred = false
    }

    /**
     * On componentWillMount fetch the data
    */
    componentWillMount () {
        this.fetchData()
    }

    /**
     * Add Listener of scroll, undo action and network
    */
    componentDidMount () {
        window.addEventListener('scroll', this.onScrollHandler.bind(this))
        window.addEventListener('online', this.networkUpdated.bind(this))
        window.addEventListener('offline', this.networkUpdated.bind(this))
        window.addEventListener('snackbar-undo', this.undoEventHandler.bind(this))
    }

    /**
     * Remove all listener to cleanup
    */
    componentWillUnmount () {
        window.removeEventListener('scroll', this.onScrollHandler.bind(this))
        window.removeEventListener('online', this.networkUpdated.bind(this))
        window.removeEventListener('offline', this.networkUpdated.bind(this))
        window.removeEventListener('snackbar-undo', this.undoEventHandler.bind(this))
    }

    /**
     * Undo Event Handler, so add the message back at same place
     * @param {Event} event
     */
    undoEventHandler (event) {
        if (event.detail) {
            this.state.messages.splice(event.detail.index, 0, event.detail)
            this.setState({messages: this.state.messages})
        }
    }

    /**
     * On network update, remove the error state
    */
    networkUpdated () {
        if (navigator.onLine) {
            this.isErrorOccurred = false
        }
    }

    /**
     * Fetch the data using API
     * @param {Int} limit
     */
    fetchData (limit = 30) {
        if (!this.isErrorOccurred) {
            fetchMessages(limit, this.state.pageToken).then((data) => {
                this.setState({messages: this.state.messages.concat(data.messages), pageToken: data.pageToken, loadingData: false})
            }).catch(() => {
                launchSnackbar('An Error occurred while fetching messages')
                this.isErrorOccurred = true
                // Retry after 30 seconds
                setTimeout(() => {
                    this.isErrorOccurred = false
                    this.setState({loadingData: true})
                    this.fetchData()
                }, 30000)
                this.setState({loadingData: false})
            })
        }
    }

    /**
     * Handler for scroll and if we have left very few messages to read then fetch more messages
    */
    onScrollHandler () {
        if ((window.innerHeight + window.scrollY + window.innerHeight) >= document.body.offsetHeight) {
            if (!this.state.loadingData) {
                this.fetchData()
                if (!this.isErrorOccurred) {
                    this.setState({loadingData: true})
                }
            }
        }
    }

    /**
     * Remove Message handler
     * @param {Int} index
     */
    removeMessage (index) {
        const message = this.state.messages.splice(index, 1)
        if (message.length) {
            message[0].index = index
            launchSnackbar(`${message[0].author.name}'s Message Deleted`, true, 5000, message[0])
            setTimeout(() => {
                this.setState({messages: this.state.messages})
            }, 500)
        }
    }

    /**
     * JSX for the messages
    */
    getMessages () {
        return this.state.messages.map((message, index) => {
            return (<Card key={message.id} swipeEnabled removeCard={() => this.removeMessage(index)}>
                <div className='card__row'>
                    <img className='card__image' src={message.author.photoUrl} />
                    <div className='card__name'>{message.author.name}</div>
                    <div className='card__update'>{message.updated}</div>
                </div>
                <p>{message.content}</p>
            </Card>)
        })
    }

    /**
     * Render Component
    */
    render () {
        const messages = this.getMessages()
        return (
            <div className='page'>
                {messages}
                {this.state.loadingData ? <div className='loader' /> : null}
            </div>
        )
    }
}
