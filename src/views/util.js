import Hammer from 'hammerjs'

/**
 * Method to check dom has the passed class or not
 * @param {Dom} ele
 * @param {String} cls
 */
export function hasClass (ele, cls) {
    return !!ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'))
}

/**
 * Add class to the dom element
 * @param {Dom} ele
 * @param {String} cls
 */
export function addClass (ele, cls) {
    if (!hasClass(ele, cls)) ele.className += ' ' + cls
}

/**
 * Remove Class from dom elemnet
 * @param {Dom} ele
 * @param {String} cls
 */
export function removeClass (ele, cls) {
    if (hasClass(ele, cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)')
        ele.className = ele.className.replace(reg, ' ')
    }
}

/**
 * Get the time difference from provided date/time to current date/time
 * @param {Date} previousDate(Date)
 */
export function timeDifference (previousDate = new Date()) {
    var msPerMinute = 60 * 1000
    var msPerHour = msPerMinute * 60
    var msPerDay = msPerHour * 24
    var msPerMonth = msPerDay * 30
    var msPerYear = msPerDay * 365

    var elapsed = new Date() - new Date(previousDate)

    if (elapsed < msPerMinute) {
        return Math.round(elapsed / 1000) + ' seconds ago'
    } else if (elapsed < msPerHour) {
        return Math.round(elapsed / msPerMinute) + ' minutes ago'
    } else if (elapsed < msPerDay) {
        return Math.round(elapsed / msPerHour) + ' hours ago'
    } else if (elapsed < msPerMonth) {
        return Math.round(elapsed / msPerDay) + ' days ago'
    } else if (elapsed < msPerYear) {
        return Math.round(elapsed / msPerMonth) + ' months ago'
    } else {
        return Math.round(elapsed / msPerYear) + ' years ago'
    }
}

/**
 * Function to launch Snackbar
 * @param {String} message('')
 * @param {Boolean} autoHide(true)
 * @param {Int} autoHideTime(5000)
 * @param {Object} undoData
 */
export function launchSnackbar (message = '', autoHide = true, autoHideTime = 5000, undoData) {
    var event = new window.CustomEvent('snackbar', { detail: {
        isSnackbarOpen: true,
        snackbarMessage: message,
        snackbarAutoHide: autoHide,
        snackbarAutoHideTime: autoHideTime,
        snackbarUndoData: undoData
    }})
    window.dispatchEvent(event)
}

/**
 * Function to close Snackbar
*/
export function closeSnackbar () {
    var event = new window.CustomEvent('snackbar', { detail: {
        isSnackbarOpen: false,
        snackbarMessage: '',
        snackbarAutoHide: false,
        snackbarAutoHideTime: 10
    }})
    window.dispatchEvent(event)
}

/**
 * Create the Request Animation function for smooth transition
*/
export const reqAnimationFrame = (function () {
    return window[Hammer.prefixed(window, 'requestAnimationFrame')] || function (callback) {
        window.setTimeout(callback, 1000 / 60)
    }
})()
