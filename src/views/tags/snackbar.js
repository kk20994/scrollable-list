import {h, Component} from 'preact'

/**
 * Snackbar class to show Error messages or any actions to perform by user
*/
export default class Snackbar extends Component {
    /**
     * Constructor of the class
     * @param {*} args
     */
    constructor (...args) {
        super(args)
        this.state = {
            snackbarAutoHide: true, // Auto hide the Snackbar or not
            snackbarAutoHideTime: 1000, // Time for auto hide of Snackbar
            isSnackbarOpen: false, // Is Snackbar open
            snackbarMessage: '' // Snackbar message
        }
        this.timeout = null
        this.handleUndo = this.handleUndo.bind(this)
    }

    /**
     * componentDidMount - Add Event for snackbar and call handler
     * Used Events Instead of Redux/Flux, as its easier to perform any action
    */
    componentDidMount () {
        window.addEventListener('snackbar', this.snackbarHandler.bind(this))
    }

    /**
     * componentWillUnmount - Remove Event for snackbar for cleanup
    */
    componentWillUnmount () {
        window.addEventListener('snackbar', this.snackbarHandler.bind(this))
    }

    /**
     * Handler for Undo action - raise event for this so other component can listen
    */
    handleUndo () {
        var event = new window.CustomEvent('snackbar-undo', {detail: this.state.snackbarUndoData})
        window.dispatchEvent(event)
        this.setState({isSnackbarOpen: false})
    }

    /**
     * Snackbar handler to show and hide
     * @param {Event} event
     */
    snackbarHandler (event) {
        if (event.detail.isSnackbarOpen && event.detail.snackbarAutoHide && event.detail.snackbarAutoHideTime) {
            if (this.timeout) {
                clearTimeout(this.timeout)
            }
            this.timeout = setTimeout(() => {
                this.setState({isSnackbarOpen: false})
            }, event.detail.snackbarAutoHideTime)
        }
        this.setState({...event.detail})
    }

    /**
     * Render component
    */
    render () {
        return (
            <div className={this.state.isSnackbarOpen ? 'snackbar snackbar--opened' : 'snackbar'}>
                <div>
                    <div className='snackbar__message'>
                        <span>{this.state.snackbarMessage}</span>
                        {this.state.snackbarUndoData ? <span className='btn btn--snackbar' onClick={this.handleUndo}>
                            UNDO
                        </span> : null}
                    </div>
                </div>
            </div>
        )
    }
}
