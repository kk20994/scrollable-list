import { h, Component } from 'preact'
import Header from './header'
import Snackbar from './snackbar'
import * as util from '../util'

/**
 * Layout class for the UI
*/
export default class Layout extends Component {
    /**
     * Add Listeners for Network update
    */
    componentDidMount () {
        window.addEventListener('online', this.networkUpdated.bind(this))
        window.addEventListener('offline', this.networkUpdated.bind(this))
    }

    /**
     * Remove Listeners to cleanup
    */
    componentWillUnmount () {
        window.removeEventListener('online', this.networkUpdated.bind(this))
        window.removeEventListener('offline', this.networkUpdated.bind(this))
    }

    /**
     * Handle Network update, launch Snackbar with the message
    */
    networkUpdated () {
        if (!window.navigator.onLine) {
            util.launchSnackbar('You are offline, Please connect to Network', false)
        } else {
            util.launchSnackbar('You are now online')
        }
    }

    /**
     * Render component
    */
    render () {
        return (
            <div id='app'>
                <Header />
                <main id='content'>
                    {this.props.children}
                </main>
                <Snackbar />
            </div>
        )
    }
}
