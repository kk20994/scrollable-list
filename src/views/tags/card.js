import { h, Component } from 'preact'
import Hammer from 'hammerjs'
import * as util from '../util'

/**
 * Card view class
 * Additional params can be passed to remove the Card
*/
export default class Card extends Component {
    /**
     * Constructor of the class
     * @param {*} args
     */
    constructor (...args) {
        super(args)
        this.translateX = 0
        this.startX = 0
        this.ticking = false
        this.timeout = null
    }

    /**
     * If Swipe is Enabled then initialize the hammer
    */
    componentDidMount () {
        if (this.props.swipeEnabled) {
            const options = {
                direction: Hammer.DIRECTION_HORIZONTAL
            }
            this.hammer = Hammer(this._slider, options)
            this.hammer.on('panstart panright panend', this.onPan.bind(this))
        }
    }

    /**
     * Remove Hammer Events listener to cleanup
    */
    componentWillUnmount () {
        if (this.props.swipeEnabled) {
            this.hammer.off('panstart panright panend', this.onPan.bind(this))
        }
    }

    /**
     * Update the element transform values
    */
    updateElementTransform () {
        var value = [
            'translate3d(' + this.translateX + 'px, 0px, 0)'
        ]

        value = value.join(' ')
        if (this._slider) {
            this._slider.style.webkitTransform = value
            this._slider.style.mozTransform = value
            this._slider.style.transform = value
        }
        this.ticking = false
    }

    /**
     * Request the Element update
    */
    requestElementUpdate () {
        if (!this.ticking) {
            util.reqAnimationFrame(this.updateElementTransform.bind(this))
            this.ticking = true
        }
    }

    /**
     * Reset the Card position
    */
    reset () {
        this.translateX = 0
        this.startX = 0
        if (this._slider) {
            util.removeClass(this._slider, 'card--swipe')
            util.removeClass(this._slider, 'card--going-deleted')
            this._slider.style.webkitTransform = ''
            this._slider.style.mozTransform = ''
            this._slider.style.transform = ''
        }
    }

    /**
     * On pan perform the swipe
     * @param {Event} ev
     */
    onPan (ev) {
        // If its swipe start the set the initial value of StartX
        if (ev.type === 'panstart') {
            this.startX = this.translateX
        }

        // Update translateX with delta
        this.translateX = this.startX + (ev.deltaX)

        // If card is swipped more then 25% and event is swipe end then delete the card
        if (this.translateX > (window.innerWidth / 3.5) && this.props.removeCard && ev.type === 'panend') {
            util.addClass(this._slider, 'card--deleted')
            this.translateX = window.innerWidth * 2
            this.requestElementUpdate()
            this.props.removeCard()
        } else if (ev.type === 'panend') { // If swipe end call and previous condition didn't meet then reset the flag
            this.reset()
        } else if (ev.type === 'panright') { // When you continue to swipe right then add class card--swipe which will reduce opacity of the flag
            util.addClass(this._slider, 'card--swipe')
            this.requestElementUpdate()
        }
        if (this.timeout) { // Clear previous timeout
            clearTimeout(this.timeout)
        }
        if (this.translateX > (window.innerWidth / 3.5) && this.props.removeCard) { // If user swipe more then 25% then reduce more opacity
            util.addClass(this._slider, 'card--going-deleted')
        }
        this.timeout = setTimeout(this.reset.bind(this), 1000) // If user swipe and holds the card then reset after 1 second
    }

    /**
     * Render the component
    */
    render () {
        return <div className='card' ref={(el) => {
            this._slider = el
            if (el) {
                this.reset()
            }
        }}>{this.props.children}</div>
    }
}
