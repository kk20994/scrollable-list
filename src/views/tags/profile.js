import { h } from 'preact'

/**
 * Profile View
 * @param {Object} props
 */
export default function (props) {
    return (
        <div className='profile'>
            <div className='profile__header' />
            <div className='profile__content'>
                <div className='profile__image'>
                    <div>
                        <img src={props.img} />
                    </div>
                </div>
                <div className='profile__title'>
                    <h2>{this.props.name}</h2>
                    <span>{this.props.title}</span>
                    <p>{this.props.subTitle}</p>
                </div>
            </div>
        </div>
    )
}
