import { h, Component } from 'preact'
import { Match } from 'preact-router/match'
import { Link } from 'preact-router'
import FaBars from 'preact-icons/fa/bars'
let prevPath

/**
 * Header Class of the APP
 * Adds fixed headed to the page, you will get sidebar only at low resolution
 * Menu options added to header or Side Bar
*/
export default class Header extends Component {
    /**
     * Constructor of the class
     * @param {*} args
     */
    constructor (...args) {
        super(args)
        this.state = {
            sidebarOpened: false
        }
        this.openSideBar = this.openSideBar.bind(this)
    }

    /**
     * Add Event listener of click on body
    */
    componentDidMount () {
        document.body.addEventListener('click', this.bodyClickHandler.bind(this))
    }

    /**
     * Remove Event listener to cleanup
    */
    componentWillUnmount () {
        document.body.removeEventListener('click', this.bodyClickHandler.bind(this))
    }

    /**
     * Open Side bar
     * @param {Event} e
     */
    openSideBar (e) {
        this.setState({sidebarOpened: true})
        e.stopPropagation()
    }

    /**
     * Close Side bar on click of body
    */
    bodyClickHandler () {
        if (this.state.sidebarOpened) {
            this.setState({sidebarOpened: false})
        }
    }

    /**
     * Render Component
    */
    render () {
        const navLinks = <nav>
            <Link href='/'>Home</Link>
            <Link href='/messages'>Messages</Link>
            <Link href='/about'>About Me</Link>
        </nav>
        const label = <Match path='/'>
            {({ matches, path, url }) => {
                if (prevPath !== path && this.state.sidebarOpened) { // hack to close side bar when we change path
                    this.setState({sidebarOpened: false})
                }
                prevPath = path
                return (path.indexOf('/messages') > -1 ? <h1>Messages</h1>
                    : (path.indexOf('/about') > -1 ? <h1>About Me</h1> : <h1>Home</h1>))
            }}
        </Match>
        return (
            <header className='header'>
                <span className='header__sidebar'>
                    <span className={this.state.sidebarOpened ? 'mask-modal mask-modal--opened' : 'mask-modal'} />
                    <span className='btn btn--icon' aria-role='button' aria-label='Open Sidebar' onClick={this.openSideBar}>
                        <FaBars size='20' color='white' />
                    </span>
                    {label}
                    <span className={this.state.sidebarOpened ? 'header__sidebar--content header__sidebar--opened' : 'header__sidebar--content'}>
                        <div className='header__sidebar--top'>
                            <h1>Scroll List</h1>
                        </div>
                        {navLinks}
                    </span>
                </span>
                <span className='header__menubar'>
                    <h1>Scroll List</h1>
                    {navLinks}
                </span>
            </header>
        )
    }
}
