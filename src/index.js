import { render } from 'preact'
import GAnalytics from 'ganalytics'
import './index.sass'

let elem, App
/**
 * Initiate the app
*/
function init () {
    App = require('./views').default
    const dom = window.document.getElementById('root')
    dom.innerHTML = '' // Clear the loading icon
    elem = render(App, dom, elem)
}

// Initiate initialization of app
init()

if (process.env.NODE_ENV === 'production') {
    // cache all assets if browser supports serviceworker
    if ('serviceWorker' in navigator && window.location.protocol === 'https:') {
        navigator.serviceWorker.register('/sw.js')
    }

    // add Google Analytics (User should update this)
    window.ga = new GAnalytics('UA-XXXXXXXX-X')
} else {
    // use preact's devtools
    require('preact/devtools')
    // listen for HMR
    if (module.hot) {
        module.hot.accept('./views', init)
    }
}
